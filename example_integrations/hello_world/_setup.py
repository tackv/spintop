import os
import sys
import subprocess
from glob import glob

from setuptools import setup, find_packages, Command
from distutils.command.build import build as _build

# This class handles the pip install mechanism.
class build(_build):  # pylint: disable=invalid-name
    """A build command class that will be invoked during package install.
    The package built using the current setup.py will be staged and later
    installed in the worker using `pip install package'. This class will be
    instantiated during install for this specific scenario and will trigger
    running the custom commands specified.
    """
    sub_commands = _build.sub_commands + [('CustomCommands', None)]
    
CUSTOM_COMMANDS = [
    ['echo', 'Custom command worked!']
]


class CustomCommands(Command):
    """A setuptools Command class able to run arbitrary commands."""
    description = 'run custom commands'
    user_options = []
      
  
    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def RunCustomCommand(self, command_list):
        print('Running command: %s' % command_list)
        p = subprocess.Popen(
            command_list,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        # Can use communicate(input='y\n'.encode()) if the command run requires
        # some confirmation.
        stdout_data, _ = p.communicate()
        print('Command output: %s' % stdout_data.decode())
        if p.returncode != 0:
            raise RuntimeError(
                'Command %s failed: exit code: %s' % (command_list, p.returncode))

    def run(self):
        for command in CUSTOM_COMMANDS:
            self.RunCustomCommand(command)
        
setup(
    name='tackv-hello-world',
    version='0.1.0',
    description='Hello World Template for spintop',
    author='William Laroche',
    author_email='william.laroche@tackv.ca',
    maintainer='William Laroche',
    maintainer_email='william.laroche@tackv.ca',
    include_package_data=True,
    package_dir={'tackv-hello-world': ''},
    packages=['tackv-hello-world'],
    # package_data={},
    # install_requires=[
    # ],
    # extras_require={
    # },
    # setup_requires=[
    # ],
    # tests_require=[
    # ],
    cmdclass={
        # Command class instantiated and run during pip install scenarios.
        'build': build,
        'CustomCommands': CustomCommands,
    }
)