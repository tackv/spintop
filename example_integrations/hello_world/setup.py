"""
Jinja2 Template to generate a setup.py for a package.
Args:
    name: Package name
    version: Package version
    setup_kwargs: Dict of additionnal setup kwargs.
"""

from setuptools import setup
        
setup_kwargs = {'description': 'Hello World Template for spintop', 'author': 'William Laroche', 'author_email': 'william.laroche@tackv.ca', 'maintainer': 'William Laroche', 'maintainer_email': 'william.laroche@tackv.ca'}

setup(
    name='tackv-hello-world',
    version='0.1.0',
    include_package_data=True,
    package_dir={'tackv-hello-world': ''},
    packages=['tackv-hello-world'],
    **setup_kwargs
)