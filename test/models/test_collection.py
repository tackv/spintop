import pytest

from spintop.models import SpintopTestRecordView
from spintop.models import SpintopTreeTestRecordBuilder, PhaseFeatureRecord, value_of_feature_named

from spintop.models.view import update, DEFAULT_PLACEHOLDER


def create_deep_test_record(width, depth):
    builder = SpintopTreeTestRecordBuilder()
    builder.set_top_level_information(None, None, None)
    for this_width in range(width):
        phase = builder
        test_phases = ['phase.{}.{}'.format(this_width, this_depth+1) for this_depth in range(depth)]
        for test_name in test_phases:
            phase = phase.new_phase(test_name, outcome=True, duration=1.0)

    return builder.build()

def test_default_primitive_test_record(flat_test_record):
    view = SpintopTestRecordView()
    
    result = view.apply(flat_test_record, flatten_dict=False)
    
    assert 'test_id' in result
    
def test_tags_default_primitive_test_record(flat_test_record):
    view = SpintopTestRecordView()
    
    flat_test_record.add_tag('tag1')
    
    result = view.apply(flat_test_record, flatten_dict=False)
    
    assert result['test_id']['tags']['tag1']

def test_view_custom_feature(random_generator):
    view = SpintopTestRecordView(
        feature_mapping={
            PhaseFeatureRecord.complete_name: PhaseFeatureRecord.outcome.is_pass
        },
        include_test_id=False
    )

    for test_record in random_generator.generate(
            count=2,
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2'),
            failure_rate=0.0
        ):

        result = view.apply(test_record, flatten_dict=False)

        assert result == {
            ':phase1': True,
            ':phase2': True,
            ':measure1': None,
            ':measure2': None
        }

def test_view_sets_value_to_none_if_no_match(random_generator):
    def never_return(feat): raise AttributeError('Never returns.')
    view = SpintopTestRecordView(
        {
            'a_feature': never_return
        },
        include_test_id=False
    )

    for test_record in random_generator.generate(
            count=1,
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2'),
            failure_rate=0.0
        ):

        result = view.apply(test_record, flatten_dict=False)

        assert result['a_feature'] == None

def test_view_value_of_feature_named(random_generator):
    view = SpintopTestRecordView(
        {
            'measure1': value_of_feature_named('measure1'),
            'measure2': value_of_feature_named('measure2'),
        },
        include_test_id=False
    )
    
    for test_record in random_generator.generate(
            count=1,
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2'),
            failure_rate=0.0
        ):

        result = view.apply(test_record, flatten_dict=False)

        assert result['measure1'] is not None
        assert result['measure2'] is not None

        result = view.apply(test_record, flatten_dict=True)

        assert result[('measure1',)] is not None
        assert result[('measure2',)] is not None

def test_feature_count():
    """
    Create a tree of shape:
x (feature_count=10)
----x (feature_count=4)
|   ----x (feature_count=3)
|       ----x (...)
|           ----x
|               ----x (feature_count=0)
----x (feature_count=4)
    ----x (...)
        ----x
            ----x
                ----x (feature_count=0)

    """
    record = create_deep_test_record(width=2, depth=5)

    assert record.test_record.feature_count == 10
    for feature in record.features:
        assert feature.feature_count == 5 - feature.depth

def test_normalize_outcomes():
    record = create_deep_test_record(width=2, depth=5)

    last_phase = record.features[-1]
    last_phase.outcome.is_pass = False

    record.normalize_outcomes()

    assert not record.data.outcome
    assert record.data.outcome is not None

def test_normalize_outcome_does_not_propagate_none():
    record = create_deep_test_record(width=2, depth=5)

    last_phase = record.features[-1]
    last_phase.outcome.is_pass = None

    record.normalize_outcomes()
    
    assert record.data.outcome is not None

def test_set_features_are_ordered_by_index():
    record = create_deep_test_record(width=2, depth=5)
    
    last_phase = record.features[-1]

    # disorder
    features = record.features[:6] + (last_phase,) + record.features[6:-1]
    record.features = features

    assert record.features[-1] == last_phase
    for index, feature in enumerate(record.all_features):
        assert feature.index == index

