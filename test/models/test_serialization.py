import pytest
import datetime
import numpy as np

from spintop.models import TestRecordSummary, SpintopTestRecordCollection, get_bson_serializer, get_json_serializer, MeasureFeatureRecord

def test_json_serialization_params_polution():
    # First call should be distinct from the second.
    get_json_serializer(datetime_serializer=lambda datetime_obj: pytest.fail('This shouldnt be called!'))
    serializer2 = get_json_serializer(datetime_serializer= lambda datetime_obj: datetime_obj.timestamp())

    datetime_obj = datetime.datetime(year=2000, month=1, day=1)
    assert serializer2.serialize(datetime_obj) == datetime_obj.timestamp()

    serializer3 = get_json_serializer(datetime_serializer= lambda datetime_obj: datetime_obj)
    assert serializer2.serialize(datetime_obj) == datetime_obj.timestamp(), "New serializer pollutes old one"
    assert serializer3.serialize(datetime_obj) == datetime_obj, "New serializer is polluted by old one"

def test_dict_list_serialization_serialize_fn():
    datetime_obj_lst = [datetime.datetime(year=2000, month=1, day=1)]
    datetime_obj_dct = {'date': datetime.datetime(year=2000, month=1, day=1)}

    serializer = get_json_serializer(datetime_serializer= lambda datetime_obj: datetime_obj.timestamp())

    assert serializer.serialize(datetime_obj_lst)[0] == datetime_obj_lst[0].timestamp()
    assert serializer.serialize(datetime_obj_dct)['date'] == datetime_obj_dct['date'].timestamp()

def test_json_serializer_forth_and_back():
    serializer = get_json_serializer()
    orig = TestRecordSummary.null()
    serialized = serializer.serialize(orig)

    assert serialized['_type'] == TestRecordSummary._type

    deserialized = serializer.deserialize(TestRecordSummary, serialized)
    
    assert orig == deserialized

def test_json_serializes_datetime():
    A_DATETIME = datetime.datetime(year=2000, month=1, day=1)

    serializer = get_json_serializer()
    orig = TestRecordSummary.null()
    orig.test_id.start_datetime = A_DATETIME
    serialized = serializer.serialize(orig)

    assert serialized['test_id']['start_datetime'] == A_DATETIME.isoformat(), "Defaut datetime serialization should be UTC string"

def test_json_transforms_np_nan_to_none():
    serializer = get_json_serializer()

    dc = MeasureFeatureRecord()
    dc.value = np.nan
    serialized = serializer.serialize(dc)

    assert serialized['value_f'] == None
    assert serialized['value_s'] == None

    value = np.nan

    assert serializer.serialize(value) == None


def test_bson_does_not_serialize_datetime():
    A_DATETIME = datetime.datetime(year=2000, month=1, day=1)

    serializer = get_bson_serializer()
    orig = TestRecordSummary.null()
    orig.test_id.start_datetime = A_DATETIME
    serialized = serializer.serialize(orig)

    assert isinstance(serialized['test_id']['start_datetime'], datetime.datetime)


