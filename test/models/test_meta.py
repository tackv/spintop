import pytest

from spintop.models import DutIDRecord, TestIDRecord, MeasureFeatureRecord, PhaseFeatureRecord, OutcomeData, FeatureRecord

def test_meta_class_field_access():
    assert DutIDRecord.version.name_ == 'version'

def test_nested_field_access():
    assert TestIDRecord.dut.id.name_ == 'dut.id'

def test_nested_field_hasattr():
    record = TestIDRecord(dut=DutIDRecord(id='x'))

    assert record.hasattr_from_field(TestIDRecord.dut.id)

def test_nested_field_has_not_attr():
    not_record = DutIDRecord(id='x')
    
    assert not not_record.hasattr_from_field(TestIDRecord.dut.id)

    with pytest.raises(AttributeError):
        not_record.getattr_from_field(TestIDRecord.dut.id)

def test_nested_field_getattr():
    record = TestIDRecord(dut=DutIDRecord(id='x'))
    assert record.getattr_from_field(TestIDRecord.dut.id) == 'x'

def test_meta_class_no_field_attribute_error():
    try:
        with pytest.raises(AttributeError):
            DutIDRecord.does_not_exist
    except AttributeError as e:
        assert 'does_not_exist' in str(e)

def test_nested_field_getattr_wrong_class():
    measure = MeasureFeatureRecord(outcome=OutcomeData(is_pass=True))
    phase = PhaseFeatureRecord(outcome=OutcomeData(is_pass=True))

    measure_only_field = MeasureFeatureRecord.outcome.is_pass
    both_field = FeatureRecord.outcome.is_pass

    assert measure.hasattr_from_field(measure_only_field)
    assert not phase.hasattr_from_field(measure_only_field)

    assert measure.hasattr_from_field(both_field)
    assert phase.hasattr_from_field(both_field)

def test_nested_field_property():
    measure = MeasureFeatureRecord(outcome=OutcomeData(is_pass=True), value=1.0)

    value_field = MeasureFeatureRecord.value

    assert measure.getattr_from_field(value_field) == 1.0

def test_value_of_serialized():

    value_field = FeatureRecord.test_id.test_uuid

    assert value_field.value_of_serialized(
        {'test_id': {'test_uuid': 'hello'}}
    ) == 'hello'

