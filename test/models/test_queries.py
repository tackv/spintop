import pytest
import json
import re

from spintop.models import Query, FeatureRecord, MeasureFeatureRecord, multi_query_serialize, multi_query_deserialize

@pytest.fixture()
def complex_query():
    q = Query()
    q.name_regex_is('a')
    q.outcome_is(is_pass=True)
    q.test_uuid_any_of(['a', 'b'])
    q.test_uuid_is('x')
    q.type_any_of([FeatureRecord, MeasureFeatureRecord])
    q.type_is(FeatureRecord)
    q.dut_match(id='X', version='Y')
    return q

def test_query_serialization(complex_query):
    query_serialized = complex_query.as_dict()
    query_deserialized = Query.from_dict(query_serialized)
    
    assert complex_query == query_deserialized

def test_query_jsonifiable(complex_query):
    query_serialized = complex_query.as_dict()

    assert complex_query == Query.from_dict(json.loads(json.dumps(query_serialized)))

def test_multi_serialize(complex_query):
    other_q = Query()
    other_q.name_regex_is('b')
    
    multi_query_serialized = multi_query_serialize(
        a=complex_query,
        b=other_q
    )

    result = multi_query_deserialize(multi_query_serialized)

    assert result['a'] == complex_query
    assert result['b'] == other_q

def test_query_tag(flat_test_record):
    query = Query().tag_is(foo='bar')
    assert query._value_equals['test_id.tags.foo'] == 'bar'

def test_query_tag_regex(flat_test_record):
    query = Query().tag_regex_is(foo='bar')
    assert query._value_equals['test_id.tags.foo'] == re.compile('bar')