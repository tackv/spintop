import pytest
import os

from spintop.errors import SpintopException
from spintop.auth import AuthException
from spintop.api_client import SpintopAPIClientModule

from mock import patch

@pytest.fixture(scope="session", autouse=True)
def spintop_api_module_from_config(auth_module, integration_config):
    if not integration_config:
        return None
    else:
        return SpintopAPIClientModule(integration_config.spintop_api_url, auth=auth_module)

@pytest.mark.integration
@pytest.mark.dependency()
def test_login(auth_module, integration_config):
    integration_config.login_and_assert(auth_module)

@pytest.mark.integration
def test_credentials_stored(auth_module, integration_config):
    credentials = auth_module.credentials
    integration_config.assert_credentials_good(credentials)

@pytest.mark.integration
@pytest.mark.dependency(depends=["test_login"])
def test_refuse_second_login(auth_module, integration_config):
    assert auth_module.credentials
    assert auth_module.credentials.get('username') == integration_config.username
    
    with pytest.raises(AuthException):
        integration_config.login(auth_module)

@pytest.mark.integration
def test_private_api_endpoint(spintop_api_module):
    spintop_api_module.test_private_endpoint()
    
@pytest.mark.integration
def test_api_from_config(spintop_api_module_from_config):
    spintop_api_module_from_config.test_private_endpoint()

@pytest.mark.integration
def test_bad_access_token_causes_refresh(auth_module, spintop_api_module):
    corrupt_token(auth_module)
    spintop_api_module.test_private_endpoint()
    
def corrupt_token(auth_module, name='access_token'):
    auth_module.credentials[name] = auth_module.credentials.get(name)[0:-4]

@pytest.mark.integration
def test_bad_access_and_bad_refresh(auth_module, spintop_api_module):
    # re-corrupt access
    corrupt_token(auth_module)
    # corrupt refresh
    corrupt_token(auth_module, name='refresh_token')
    
    with pytest.raises(SpintopException):
        spintop_api_module.test_private_endpoint()

@pytest.mark.integration
def test_logout(auth_module, integration_config):
    integration_config.assert_credentials_good(auth_module.credentials)
    auth_module.logout()
    integration_config.assert_credentials_empty(auth_module.credentials)
    
    
    
