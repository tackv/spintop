import pytest
import types

from spintop.errors import SpintopException
from spintop.api_client.base import SpintopAPIError

from spintop.models import Query
from spintop.generators.random_gen import RandomTestGenerator
from spintop.transforms import ForceTestbenchNameTransformer

AN_UNAUTHORIZED_ORG = 'any_unauthorized_org_key'

@pytest.fixture(scope="session")
def spintop_api_tests(spintop_api_module, integration_config):
    
    # Check that auth works
    with pytest.raises(SpintopAPIError):
        spintop_api_module.get_org_info(AN_UNAUTHORIZED_ORG)

    # Check that user org is as expected (tests data is bound to test org)
    actual_user_org = spintop_api_module.get_org_info(integration_config.EXPECTED_ORG)

    integration_config.assert_org_match(actual_user_org)

    # Check that no tests are available    
    # stored_tests = spintop_api_module.tests.retrieve()
    # assert len(stored_tests) == 0

    try:
        yield spintop_api_module.tests
    finally:
        spintop_api_module.tests.delete()

@pytest.mark.integration
def test_login_for_persistence(auth_module, integration_config):
    integration_config.login_and_assert(auth_module)

TESTBENCH_NAMES = ('first', 'second')

def first_testbench_gen():
    return create_testbench_gen(TESTBENCH_NAMES[0])

def second_testbench_gen():
    return create_testbench_gen(TESTBENCH_NAMES[1])

def create_testbench_gen(testbench_name):
    return RandomTestGenerator() + ForceTestbenchNameTransformer(testbench_name)

GEN_COUNT = 5

@pytest.mark.integration
def test_create_tests(spintop_api_tests):
    new_tests = first_testbench_gen().generate(count=GEN_COUNT)
    new_tests = list(new_tests)
    spintop_api_tests.create(new_tests)
    stored_tests_gen = spintop_api_tests.retrieve()
    assert isinstance(stored_tests_gen, types.GeneratorType)

    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT
    stored_uuids = set(stored_tr.test_id.test_uuid for stored_tr in stored_tests)
    new_uuids = set(new_tr.test_id.test_uuid for new_tr in new_tests)

    assert stored_uuids == new_uuids

@pytest.mark.integration
def test_retrieve_all_testbench(spintop_api_tests):
    stored_tests_gen = spintop_api_tests.retrieve(query=Query().testbench_name_is(TESTBENCH_NAMES[0]))
    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT

@pytest.mark.integration
def test_generate_second_testbench(spintop_api_tests):
    new_tests = second_testbench_gen().generate(count=GEN_COUNT)
    spintop_api_tests.create(new_tests)

    stored_tests_gen = spintop_api_tests.retrieve(query=Query().testbench_name_is(TESTBENCH_NAMES[1]))
    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT

@pytest.mark.integration
def test_retrieve_all_tests(spintop_api_tests):
    stored_tests_gen = spintop_api_tests.retrieve()
    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT*2

@pytest.mark.integration
def test_update_all_tests(spintop_api_tests):
    stored_tests_gen = spintop_api_tests.retrieve()
    stored_tests = list(stored_tests_gen)
    stored_tests[0].features[0].name = 'this-is-a-new-name'

    spintop_api_tests.update(stored_tests)

    stored_tests_gen = spintop_api_tests.retrieve()
    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT*2 # Still same length
    assert stored_tests[0].features[0].name == 'this-is-a-new-name'

@pytest.mark.integration
def test_delete_one_testbench(spintop_api_tests):
    spintop_api_tests.delete(query=Query().testbench_name_is(TESTBENCH_NAMES[1]))

    stored_tests_gen = spintop_api_tests.retrieve()
    stored_tests = list(stored_tests_gen)
    assert len(stored_tests) == GEN_COUNT

@pytest.mark.integration
def test_retrieve_one_test(spintop_api_tests):
    new_tests = list(first_testbench_gen().generate(count=1))
    record = new_tests[0]

    spintop_api_tests.create(new_tests)

    test = spintop_api_tests.retrieve_one(record.test_id.test_uuid)
    assert test.test_id == record.test_id


