import os

import pytest

from spintop.transforms.source_formats.ni_atml_fmt import NiATMLFormatTransformer
from spintop.generators.files_gen import FileGenerator

from spintop.models import (
    SpintopTestRecordCollection,
    MeasureFeatureRecord,
    is_type_of
)

HERE = os.path.abspath(os.path.dirname(__file__))
TEST_FILE = os.path.join(HERE, "NI_ATML_TEST_FILE.xml")

ni_atml_file_transform = FileGenerator() + NiATMLFormatTransformer()


@pytest.fixture(scope='module')
def flat_test_record():
    return ni_atml_file_transform(TEST_FILE)

def test_measure_value_c_Value(flat_test_record):
    """c:Value is present for string values"""
    feature = flat_test_record.find_feature(
        lambda feature: is_type_of(feature, MeasureFeatureRecord) and 'R640_P_TSC240-7_A_AC_Tx3_3000MHz_Harmonic2 [dBc]' in feature.name and 'ReportText' in feature.name
    )
    
    assert feature.value == "Value has been compensated for routing loss (-0.99 )"
    
def test_measure_value_in_tag(flat_test_record):
    """@value is present for float and other numerical values"""
    feature = flat_test_record.find_feature(
        lambda feature: is_type_of(feature, MeasureFeatureRecord) and 'R640_P_TSC240-7_A_AC_Tx3_3000MHz_Harmonic2 [dBc]' in feature.name and 'Numeric' in feature.name
    )
    
    assert feature.value == 22.3470523452
    
