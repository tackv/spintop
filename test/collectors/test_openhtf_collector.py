import pytest

def test_indices_match(flat_test_record):
    """Make sure that the position in features array matches with the feature's index attribute.
    
    I.e. assert (flat_test_record.features[x].index == x)"""
    for index, feature in enumerate(flat_test_record.all_features):
        assert index == feature.index, "Feature index {} does not match with position in array {}.".format(feature.index, index)
