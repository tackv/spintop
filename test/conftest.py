import pytest
import pytest_dependency # noqa
import os

from datetime import datetime
from integration_config import IntegrationConfig

from spintop import Spintop

from spintop.auth import AuthModule, InMemoryCredentialsStore
from spintop.api_client import SpintopAPIClientModule, SpintopAPISpecAuthBootstrap

from spintop.generators.random_gen import RandomTestGenerator
from spintop.models import SpintopTestRecordCollection

from multiprocessing import Process

HERE = os.path.abspath(os.path.dirname(__file__))

@pytest.fixture(scope="session")
def docker_compose_file():
    return os.path.join(HERE, "docker-compose.yml")

@pytest.fixture(scope="session")
def random_generator():
    return RandomTestGenerator()

@pytest.fixture(scope="session")
def auth_module(integration_config):
    return AuthModule(
        auth_bootstrap=SpintopAPISpecAuthBootstrap(integration_config.spintop_api_url) if integration_config else None,
        credentials_store=InMemoryCredentialsStore()
    )

@pytest.fixture(scope="session", autouse=True)
def spintop_api_module(auth_module, integration_config):
    if not integration_config:
        return None
    else:
        return SpintopAPIClientModule(integration_config.spintop_api_url, auth=auth_module)

@pytest.fixture(scope="module")
def raw_collection(random_generator):
    raw_collection = SpintopTestRecordCollection(
        random_generator.generate(
            count=2, 
            start_datetime=datetime(year=2020, month=1, day=1), 
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2')
        )
    )
    return raw_collection

@pytest.fixture(scope="module")
def flat_record_collection(raw_collection):
    return raw_collection

@pytest.fixture()
def flat_test_record(flat_record_collection):
    return flat_record_collection.records[0]

def pytest_addoption(parser):
    parser.addoption(
        "--integration-file", action="store", default=None, help="The yml file containing the integration parameters such as the URL of the API."
    )

def pytest_configure(config):
    config.addinivalue_line("markers", "integration: mark test as auth/api integration test")

def pytest_collection_modifyitems(config, items):
    if config.getoption("--integration-file"):
        # --integration-file given in cli: do not skip integration tests
        return
    skip_slow = pytest.mark.skip(reason="need --integration-file option to run")
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_slow)

def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item

def pytest_runtest_setup(item):
    previousfailed = getattr(item.parent, "_previousfailed", None)
    if previousfailed is not None:
        pytest.xfail("previous test failed (%s)" % previousfailed.name)

@pytest.fixture(scope="session")
def integration_config(request):
    filepath = request.config.getoption("--integration-file")
    if filepath:
        config = IntegrationConfig.load_file(filepath)
    else:
        config = None
    return config

@pytest.fixture()
def spintop():
    return Spintop()

@pytest.fixture()
def flat_test_record(flat_record_collection):
    return flat_record_collection.records[0]