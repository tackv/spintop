import pytest
import numpy as np
from contextlib import contextmanager
from datetime import datetime

from sqlalchemy import create_engine
import mongomock

import docker as docker_client
import psycopg2


from spintop.models import TestRecordSummary, SpintopTestRecordCollection, Query, MeasureFeatureRecord

from spintop.persistence.mongo import MongoPersistenceFacade
from spintop.persistence.postgresql import PostgreSQLPersistenceFacade

@pytest.fixture()
def test_collection(random_generator):
    """ Generates a test collection with 2 records with each 4 features: 2 phases and 2 measures.
    """
    raw_collection = SpintopTestRecordCollection(
        random_generator.generate(
            count=2, 
            start_datetime=datetime(year=2020, month=1, day=1), 
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2')
        )
    )
    return raw_collection

@pytest.fixture(scope="session")
def mongo_db():
    return mongomock.MongoClient().test_db

@pytest.fixture(scope="session")
def sql_engine():
    return create_engine('postgresql://localhost')

build_facade = {
    MongoPersistenceFacade: lambda **kwargs: MongoPersistenceFacade(kwargs['mongo_db']),
    PostgreSQLPersistenceFacade: lambda **kwargs: PostgreSQLPersistenceFacade(kwargs['sql_engine']),
}

def engine_from_docker(docker_ip, docker_services):
    port = docker_services.port_for("postgres", 5432)
    docker_services.wait_until_responsive(
        timeout=30.0, pause=0.1, check=lambda: is_pg_responsive(docker_ip, port)
    )
    return create_engine(f'postgresql://spintop:spintop@{docker_ip}:{port}/spintop')

def is_pg_responsive(ip, port):
    try:
        with psycopg2.connect(host=ip, port=port, user="spintop", password="spintop", database="spintop") as conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT version();')
        return True
    except psycopg2.Error:
        return False

@pytest.fixture(scope='session', params=[MongoPersistenceFacade, PostgreSQLPersistenceFacade])
def persistence_facade(request, mongo_db, docker_ip, docker_services):
    sql_engine = engine_from_docker(docker_ip, docker_services)

    builder = build_facade.get(request.param)
    return builder(mongo_db=mongo_db, sql_engine=sql_engine)

def test_storage_integrity(test_collection, persistence_facade):
    datetimes = [tr.test_id.start_datetime for tr in test_collection.records]
    persistence_facade.create(test_collection.records)
    collection = SpintopTestRecordCollection(persistence_facade.retrieve(deserialize=True))
    
    assert len(collection.records) == len(test_collection.records)
    
    compare_id = 0
    record_a = collection.records[compare_id]
    record_b = test_collection.records[compare_id]
    assert len(record_a.features) == len(record_b.features)
    assert record_a.test_id == record_b.test_id
    assert datetimes == [tr.test_id.start_datetime for tr in collection.records]
    
def test_feature_only_retrieval(persistence_facade):
    if isinstance(persistence_facade, PostgreSQLPersistenceFacade):
        pytest.xfail('PostgreSQL does not support querying of features directly.')
        
    query = Query().type_is(MeasureFeatureRecord)

    collection = SpintopTestRecordCollection(persistence_facade.retrieve(query, deserialize=True))

    assert collection.records

    all_features = []
    for record in collection.records:
        all_features += record.features
        assert record.test_record.feature_count == 4

    for feature in all_features:
        assert isinstance(feature, MeasureFeatureRecord)

def test_filter_by_tag(random_generator, persistence_facade):
    raw_collection = SpintopTestRecordCollection(
        random_generator.generate(
            count=2, 
            start_datetime=datetime(year=2019, month=1, day=1),  # Different datetime for different uuid than previous tests
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2')
        )
    )

    raw_collection.records[0].test_id.add_tag('mytag', 5)
    raw_collection.records[1].test_id.add_tag('mytag', 2)
    with temporary_create(persistence_facade, raw_collection.records) as test_uuids:
        query_0 = Query().tag_is(mytag=5)
        collection = SpintopTestRecordCollection(persistence_facade.retrieve(query_0, deserialize=True))
        possibly_record_0 = collection.records[0]

        query_1 = Query().tag_is(mytag=2)
        collection = SpintopTestRecordCollection(persistence_facade.retrieve(query_1, deserialize=True))
        possibly_record_1 = collection.records[0]

        assert possibly_record_0.test_id.test_uuid == test_uuids[0]
        assert possibly_record_1.test_id.test_uuid == test_uuids[1]

@contextmanager
def temporary_create(persistence_facade, records):
    test_uuids = [tr.test_id.test_uuid for tr in records]
    try:
        persistence_facade.create(records)
        yield test_uuids
    finally:
        persistence_facade.delete(Query().test_uuid_any_of(test_uuids))

def test_update_upsert_renamed_feature(test_collection, persistence_facade):
    assert test_collection.are_records_unique()

    # Tests already exist. 
    # Should delete the old feature and create the new one.
    test_collection.records[0].features[0].name = 'new-feature-name'

    persistence_facade.update(test_collection.records)

    collection = SpintopTestRecordCollection(persistence_facade.retrieve(deserialize=True))

    assert len(collection.records) == 2

    modified_record = collection.records[0]

    assert len(modified_record.features) == 4

    assert modified_record.features[0].name == 'new-feature-name'

def test_retrieve_one_record(test_collection, persistence_facade):
    test_uuid = test_collection.records[0].test_id.test_uuid

    record = persistence_facade.retrieve_one(test_uuid, deserialize=True)

    assert record.test_id == test_collection.records[0].test_id

def test_count(test_collection, persistence_facade):
    assert len(test_collection.records) == persistence_facade.count()

def test_query_dut_match(test_collection, persistence_facade):
    record_dut_ids = [record.test_id.dut.id for record in test_collection.records]
    query = Query().dut_match(id='|'.join(record_dut_ids))
    assert len(test_collection.records) == persistence_facade.count(query)

def test_limit_range(test_collection, persistence_facade):
    expected = test_collection.records[1]

    records = list(persistence_facade.retrieve(deserialize=True, limit_range=(1, 2)))

    assert len(records) == 1
    record = records[0]

    assert len(record.features) == len(expected.features)
    assert record.test_id == expected.test_id


def test_nan_measure_values(random_generator, persistence_facade):
    """Since nan are not universally supported, spintop stores them as None values."""
    raw_collection = SpintopTestRecordCollection(
        random_generator.generate(
            count=1, 
            start_datetime=datetime(year=2019, month=1, day=1),  # Different datetime for different uuid than previous tests
            test_phases=('phase1', 'phase2'),
            measures=('measure1', 'measure2')
        )
    )

    raw_collection.records[0].find_feature(lambda feat: feat.name == 'measure1').value = np.nan

    with temporary_create(persistence_facade, raw_collection.records) as test_uuids:
        query_0 = Query().test_uuid_is(test_uuids[0])
        collection = SpintopTestRecordCollection(persistence_facade.retrieve(query_0, deserialize=True))
        record_0 = collection.records[0]

        value = record_0.find_feature(lambda feat: feat.name == 'measure1').value
        assert value is None or value is np.nan
    
