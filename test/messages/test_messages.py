from spintop.messages import TestsUpdateMessage
from spintop.messages.schemas import get_schema
from spintop.env import SpintopEnv

def test_serialize():
    env = SpintopEnv()
    message = TestsUpdateMessage.create(delete_all=True)
    message.env = env.freeze_database_access_only()

    schema = get_schema(TestsUpdateMessage)
    dumped = schema.dump(message)
    loaded = schema.load(dumped)

    assert loaded == message
