class OldSpinhubAPI:
    
    def get_this_machine_id(self):
        return self.config.get_selected_profile()['machine_id']
    
    def get_token(self):
        token_url = self._get_org_urls('token')
        return self.session.get(token_url).text
    
    @property
    def machine(self):
        if self._machine is None:
            machines_url = self._get_org_urls('machines')
            this_machine_id = self.get_this_machine_id()
            logger.info('Local machine ID is %s' % this_machine_id)
            self._machine = self.session.get(machines_url + '/' + this_machine_id).json()
        return self._machine
    

    def create_task(self, task_type, user_id=None):
        if user_id is None:
            user_id = self.get_this_machine_id()
        machines_url = self._get_org_urls('machines')
        self.session.post(machines_url + '/' + user_id + '/tasks', json=dict(
            task_type=task_type
        ))
    
    def task_acknowledged(self, task_id):
        return self.update_task_status(task_id, 'ACKNOWLEDGED')
    
    def task_started(self, task_id):
        return self.update_task_status(task_id, 'IN_PROGRESS')
    
    def task_done(self, task_id, success=True):
        return self.update_task_status(task_id, 'DONE', task_success=success)
        
    def update_task_status(self, task_id, task_status, task_success=None):
        user_id = self.get_this_machine_id()
        machines_url = self._get_org_urls('machines')
        self.session.put(machines_url + '/' + user_id + '/tasks', json=dict(
            uuid=task_id,
            task_status=task_status,
            task_success=task_success
        ))
        
    
    def get_tasks(self, **query_params):
        machines_url = self._get_org_urls('machines')
        this_machine_id = self.get_this_machine_id()
        return self.session.get(machines_url + '/' + this_machine_id + '/tasks', params=query_params).json()
    
    def _get_org_urls(self, sub_key):
        raise NotImplementedError('Use self.get_link instead.')
        # return self.default_org['_links'][sub_key]

    def register_machine(self, name, token, org_id=None):
        if org_id:
            machines_url = '/orgs/%s/machines' % org_id
        else:
            machines_url = self._get_org_urls('machines')
            
        logger.info('Registering machine')
        
        params = {}
        if token:
            params.update({'secret_code': token})
        
        response = self.session.put(machines_url + '/register', json=dict(
            nickname= name,
            hostname= platform.node(),
            client_version=VERSION
        ), params=params)
        
        data = response.json()
        self.config.update_profile(machine_id= data['uuid'])
        self.config.save()
    
    def upload_file(self, object_key, version, filepath, checksums={}):
        objects_url = self._get_org_urls('objects')
        logger.info('Asking the Spintop API to upload new file %s @ %s' % (filepath, object_key))
        # Generate a new URL for upload.
        
        _, filename = os.path.split(filepath)
        response = self.session.put(objects_url, json=dict(
            object_key=object_key,
            version_name=version,
            filename=filename,
            checksums=checksums
        ))
        response_data = response.json()
        upload_uri = response_data['upload_uri']
        upload_fields = response_data['upload_fields']
        
        logger.info('Beginning upload at Spintop API provided URI')
        with open(filepath, 'rb') as file_to_upload:
            files = {'file': file_to_upload}
            requests.post(upload_uri, files=files, data=upload_fields)
        
        self.confirm_uploaded_file(response_data)
        return response_data
    
    def confirm_uploaded_file(self, upload_response):
        confirm_uri = self.get_data_blob_uri(upload_response['data_blob'], key='confirm')
        self.session.put(confirm_uri)
    
    def download_uploaded_file(self, upload_response, stream):
        download_uri = self.get_data_blob_uri(upload_response['data_blob'])
        return self.download_data_blob(download_uri, stream)
    
    def get_data_blob_uri(self, data_blob, key='download'):
        return data_blob['_links'][key]
    
    def get_tag_download_uri(self, tag):
        return tag['_links']['download']
    
    def download_file(self, object_key, target_folder=TEMPORARY_DOWNLOAD_FOLDER, index=0):
        objects_url = self._get_org_urls('objects')
        response = self.session.get(objects_url + '/' + object_key)
        object_details = response.json()
        tag = object_details['tags'][index]
        
        logger.debug(pformat(tag))
        logger.info('Downloading {object_key} [version={tagname}][{index}]'.format(
            object_key=object_key,
            tagname=tag['tagname'],
            index=index
        ))
        download_uri = self.get_tag_download_uri(tag)
        
        return self.download_data_blob(download_uri, target_folder=target_folder)
        
    def download_data_blob(self, download_uri, target_folder=None, stream=None):
        data_object = self.session.get(download_uri).json()
        url = data_object['download_uri']
        filename = data_object['filename']
        resp = requests.get(url, allow_redirects=True)
        
        if stream:
            stream.write(resp.content)
            return None
        else:
            filepath = os.path.join(target_folder, filename)
            with open(filepath, 'wb+') as writefile:
                writefile.write(resp.content)
            return filepath
