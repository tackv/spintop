pytest
pytest-cov
pytest-dependency
pytest-docker

docker
docker-compose

mock
pandas
pymongo
mongomock
sqlalchemy
psycopg2
-e .
