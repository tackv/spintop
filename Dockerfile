FROM python:3.7

COPY . /app
WORKDIR /app

RUN python -m pip install -e .
RUN python -m pip install -r test_requirements.txt

CMD pytest test/ --integration-file test/secrets.yml