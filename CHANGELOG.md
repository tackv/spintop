# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0]

### Added

- PersistenceFacade for PostgreSQL, available at `spintop.persistence.postgresql`. 
- Generic analytics stream support to PostgreSQL, available at `spintop.persistence.singer_postgresql`. Based on Singer Postgres Target. 
- `spintop.env.SpintopEnv` to centralize all env variables used by Spintop.
- Adds authentication using oauth device flow, which is more user friendly and secure than the password-in-cli flow. For headless devices, the verification URL appears in the cli output.

### Changed
- SpintopTestRecordView now accounts for defaults. If a key from the mapping resolves at one point during view creation, the value is will at least be None if no values ever resolve.
- SpintopTestRecordView can now process serialized records, although care must be taken to use get_recursive or similar functions to access attributes. I.e. The native `record.test_id.dut.id` should be replaced by `record.get_recursive(TestRecordSummary.test_id.dut.id.name_)`.
- The config module was replaced by a credentials store only. The credentials file is managed similarly to how Google manages the gcloud credentials: the path to the file can be overwritten using the SPINTOP_CREDENTIALS_FILE env variable. This leverages the new SpintopEnv object.
- Split access token decoding into a specific class `from spintop.api_client import SpintopAccessTokenDecoder` to allow API and client to share the same access token payload decoding mechanics.

### Deprecated
- The model structure refactoring moved the `spintop.models` subpackages `internal`, `collection` and `tree_struct` into the `spintop.models.test_records` package. All those imports should be accessible using `spintop.models`, e.g. `from spintop.models.internal import DutIDRecord` becomes `from spintop.models import DutIDRecord`
- Removed the config module with its associated 'profiles'. The structure was overly complex and its statefulness provoked strange bugs on other OSs.